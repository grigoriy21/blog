<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $posts = Post::latest()->get();

        return view('posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        echo 'mmmmmm';
//        return view('posts.create');
    }

    public function store()
    {
//        dd(request(['title','body']));

        //Create a new post using the request data
//        $post = new Post();
//
//        $post->title = request('title');
//        $post->body = request('body');
//
//        //Save it to the database
//        $post->save();
        //And then redirect

        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required',

        ]);

        auth()->user->publish(
            new Post(request(['title','body']))
        );

        Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

        return redirect('/');
    }

}
