<?php

//use App\Task;
//
//Route::get('/tasks', 'TasksController@index');
//
//
//Route::get('/tasks/{task}', 'TasksController@show');

Route::get('/', 'PostsController@index')->name('home');
Route::get('/posts/create', 'PostsController@create');
//Route::get('/posts/create', function (){echo'hhhhhhhh';});

Route::post('/posts', 'PostsController@store');
Route::get('/posts/{post}', 'PostsController@show');

Route::get('/posts/{post}/comments', 'CommentsController@store');

Route::get('/register','RegistrationController@create');
Route::post('/register','RegistrationController@store');

Route::get('/login','SessionsController@create');
Route::get('/logout','SessionsController@destroy');

//




// controller => PostsController
// Eloquent model => Post
// migration => create_posts_table

/*posts


GET /posts

GET /posts/create

POST /posts

GET /posts/{id}/edit

GET /posts/{id}

PATCH /posts/{id}

DELETE /posts/{id}*/



